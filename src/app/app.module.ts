import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { LocalStorageModule } from 'angular-2-local-storage';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';

import { AppComponent } from './app.component';
import { SocketService } from './services/socket.service';
import { DataService } from './services/data.service';
import { AppRoutingModule } from './/app-routing.module';
import { ViewportComponent } from './components/viewport/viewport.component';

const config: SocketIoConfig = { url: 'http://192.168.178.248:9001', options: {} };

@NgModule({
	declarations: [
		AppComponent,
		ViewportComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		SocketIoModule.forRoot(config),
        LocalStorageModule.withConfig({
            prefix: 'app',
            storageType: 'localStorage'
        })		
	],
	providers: [SocketService, DataService],
	bootstrap: [AppComponent]
})
export class AppModule { }
