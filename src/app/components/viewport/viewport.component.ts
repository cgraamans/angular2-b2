import { Component, OnInit } from '@angular/core';
import {SocketService} from '../../services/socket.service'

@Component({
	selector: 'app-viewport',
	templateUrl: './viewport.component.html',
	styleUrls: ['./viewport.component.css']
})
export class ViewportComponent implements OnInit {

	constructor(private socket:SocketService){}

	ngOnInit() { }

}
