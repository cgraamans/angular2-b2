import { Injectable } from '@angular/core';
import { Socket } from 'ng-socket-io';
import { DataService } from './data.service';
import * as jwt from 'jsonwebtoken';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/map';

@Injectable()
export class SocketService {

	private token:BehaviorSubject<string> = new BehaviorSubject(null);

	constructor(private socket:Socket, private data:DataService) {

		socket.on('init',init=>{

			if(init.ok===true) {

				this.token.next(init.secret);
				let val:any = this.data.getStore('_user');
				if(val){

					if((val.apiId) && (val.token)) {

						this.emit('auth',{apiId:val.apiId,token:val.token});

					}

				}

			}

		});

		socket.on('auth',authMsg=>{

			this.credentialize(authMsg);

		});
		socket.on('auth-register',authMsg=>{

			this.credentialize(authMsg);

		});

	 }

	emit(toSocket,message, ...opts){

		let usr = this.data.getAuth();
		if((usr) && (typeof message === 'object')){
			message.user = usr;        
		}

		this.encode(message,val=>{

			this.socket.emit(toSocket,val);

		});

	}

	on(fromSocket) {

		return this.socket
			.fromEvent(fromSocket)
			.map(msg=>this.decode(msg));

	}

	private encode(message,callback) {

		let val = this.token.getValue();
		if(!val){

			setTimeout(()=>{

				this.encode(message,callback);

			},1000);

		} else {

			callback(jwt.sign(message,val));

		}

	}

	private decode(message) {

		let token = this.token.getValue();
		if(token){

			try {

				return jwt.verify(message,token);
		
			} catch(e) {
			
				return {ok:false,e:e,msg:message};
		
			}      
		
		} else {
		
			return {ok:false,e:'No valid token'};
		
		}

	}

	private credentialize(msg:any){

		let authMsg = this.decode(msg);
		if(authMsg.ok === true) {

			if(authMsg.user){

				this.data.setAuth(authMsg.user);  

			}

		} else {

			this.data.setAuth();
			if(authMsg.msg){

				if(authMsg.msg){
				
				this.error(authMsg.msg);
				
				}

			}
			if(authMsg.e){

				this.error(authMsg.e);

			}

		}

	}

	error(e) {
		console.log(e);
	}


}
