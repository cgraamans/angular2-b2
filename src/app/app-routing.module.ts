import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewportComponent } from './components/viewport/viewport.component'

const routes: Routes = [
	{ path: '', component: ViewportComponent },
	{ path: 'system/:system', component: ViewportComponent },
	{ path: 'object/:object', component: ViewportComponent },
	{ path: 'cluster/:cluster', component: ViewportComponent },
];

@NgModule({
	exports: [
		RouterModule
	],
	imports: [ 
		RouterModule.forRoot(routes) 
	],

})

export class AppRoutingModule { }
